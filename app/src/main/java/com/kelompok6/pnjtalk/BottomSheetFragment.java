package com.kelompok6.pnjtalk;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomSheetFragment extends Fragment {
    private View parentView;
    public EditText tweetText;
    SharedPreferences pref;
    RequestQueue requestQueue;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.bottom_sheet, container, false);
        pref = this.getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        requestQueue = Volley.newRequestQueue(getActivity());

        return inflater.inflate(R.layout.bottom_sheet, container, false);
    }

//    public void postTweet(View view){
//        JSONObject body = new JSONObject();
//        try {
//            body.put("user_id", pref.getString("userId",""));
//            Log.d("userIDnya", "onClick: "+pref.getString("userId",""));
//            body.put("tweet", tweet.getText());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        String uri = "http://117.53.45.240/pnjtweet-backend/api/tweets";
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
//                uri,body,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.d("", "onResponse: response = "+response.getString("tweet"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        //Failure Callback
//                        Log.d("login", "onErrorResponse: "+error);
//                    }
//                });
//        requestQueue.add(req);
//    }
}
