package com.kelompok6.pnjtalk;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import in.mayanknagwanshi.imagepicker.imageCompression.ImageCompressionListener;
import in.mayanknagwanshi.imagepicker.imagePicker.ImagePicker;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImagePickerActivity extends AppCompatActivity {
    ImagePicker imagePicker;
    SharedPreferences pref;
    RequestQueue requestQueue;
    String param;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        requestAppPermissions();
        pref = this.getSharedPreferences("MyPref",MODE_PRIVATE);
        requestQueue = Volley.newRequestQueue(this);
        param = getIntent().getExtras().getString("param");

        imagePicker = new ImagePicker();
        imagePicker.withActivity(this) //calling from activity
                .chooseFromGallery(true) //default is true
                .chooseFromCamera(true) //default is true
                .withCompression(true) //default is true
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == ImagePicker.SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
            //Add compression listener if withCompression is set to true
            imagePicker.addOnCompressListener(new ImageCompressionListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onCompressed(String filePath) {//filePath of the compressed image
                    //convert to bitmap easily
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    Log.d("", "onCompressed: "+filePath);
                    upload(selectedImage);
//                    upload(selectedImage);
                }
            });
            String filePath = imagePicker.getImageFilePath(data);
            Log.d("", "onActivityResult: filePath = "+filePath);
            if (filePath != null) {//filePath will return null if compression is set to true
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);

            }
            super.onActivityResult(requestCode,resultCode,data);
            Intent intent = new Intent(this,MainActivity.class);
            super.startActivity(intent);
        }
        else{
            Log.d("", "onActivityResult: false");
            super.onActivityResult(requestCode,resultCode,data);
            Intent intent = new Intent(this,MainActivity.class);
            super.startActivity(intent);
//            Intent intent = new Intent(this,MainActivity.class);
//            startActivity(intent);

//            Fragment fragment = new ProfileFragment();
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(MainActivity.mainView, fragment, "fragment")
//                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                    .commitAllowingStateLoss();
        }
        //call the method 'getImageFilePath(Intent data)' even if compression is set to false


    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void changeFragment(Fragment targetFragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void upload(Bitmap file){
        JSONObject body = new JSONObject();
        File f = new File(getBaseContext().getCacheDir(), "temp");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        file.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            body.put("file", f);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), f);

        String uri = "http://117.53.45.240/pnjtweet-backend/api/upload?param="+this.param+"&&id="+this.pref.getString("userId","");
        Log.d("", "upload: uri = "+uri);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                uri, body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("", "onResponse: response = "+response.getString("status"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Failure Callback
                        Log.d("postTweet", "onErrorResponse: "+error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);
                String token = pref.getString("token", "");
                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(req);
    }
}
