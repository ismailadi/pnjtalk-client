package com.kelompok6.pnjtalk.Classes;

public class Tweet {
    private String id;
    private String userId;
    private String tweet;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private String username;
    private String name;
    private String identityId;
    private String profilePicture;
    private String coverPicture;

    /**
     * No args constructor for use in serialization
     *
     */
    public Tweet() {
    }

    /**
     *
     * @param updatedAt
     * @param id
     * @param username
     * @param identityId
     * @param createdAt
     * @param profilePicture
     * @param name
     * @param userId
     * @param deletedAt
     * @param tweet
     * @param coverPicture
     */
    public Tweet(String id, String userId, String tweet, String createdAt, String updatedAt, String deletedAt, String username, String name, String identityId, String profilePicture, String coverPicture) {
        super();
        this.id = id;
        this.userId = userId;
        this.tweet = tweet;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.username = username;
        this.name = name;
        this.identityId = identityId;
        this.profilePicture = profilePicture;
        this.coverPicture = coverPicture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(String coverPicture) {
        this.coverPicture = coverPicture;
    }

}
