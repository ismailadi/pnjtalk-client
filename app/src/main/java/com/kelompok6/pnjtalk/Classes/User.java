package com.kelompok6.pnjtalk.Classes;

public class User {
    private String id;
    private String username;
    private String password;
    private String name;
    private String identityId;
    private String dateOfBirth;
    private String bio;
    private Object profilePicture;
    private Object coverPicture;
    private String createdAt;
    private String updatedAt;
    private Object deletedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }

    /**
     *
     * @param updatedAt
     * @param dateOfBirth
     * @param id
     * @param username
     * @param bio
     * @param identityId
     * @param createdAt
     * @param profilePicture
     * @param name
     * @param deletedAt
     * @param coverPicture
     * @param password
     */
    public User(String id, String username, String password, String name, String identityId, String dateOfBirth, String bio, Object profilePicture, Object coverPicture, String createdAt, String updatedAt, Object deletedAt) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.identityId = identityId;
        this.dateOfBirth = dateOfBirth;
        this.bio = bio;
        this.profilePicture = profilePicture;
        this.coverPicture = coverPicture;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Object getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Object profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Object getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(Object coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }
}
