package com.kelompok6.pnjtalk;


//import android.app.FragmentTransaction;
import android.support.v4.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    SharedPreferences pref;
    ResideMenu resideMenu;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemProfile;
    private ResideMenuItem logout;
    Context ctx;
    EditText tweet;
    Button posttweet;
    RequestQueue requestQueue;
    BottomSheetDialog dialog;
    public static int mainView = R.id.main_fragment;
    View profileFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;

        setContentView(R.layout.activity_main);

        dialog = new BottomSheetDialog(this);
        pref = this.getSharedPreferences("MyPref",MODE_PRIVATE);
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        posttweet = findViewById(R.id.posttweet);

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.pohon);
//        resideMenu.setShadowVisible(false);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        // create menu items;

        itemHome     = new ResideMenuItem(this, R.drawable.ic_dashboard,"Home");
        itemProfile  = new ResideMenuItem(this, R.drawable.ic_person_outline,  "Profile");
        logout  = new ResideMenuItem(this, R.drawable.ic_exit_to_app_black_24dp,  "Logout");

        itemHome.setOnClickListener(this);
        itemProfile.setOnClickListener(this);
        logout.setOnClickListener(this);

        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(logout, ResideMenu.DIRECTION_LEFT);

        resideMenu.setMenuListener(menuListener);

        if( savedInstanceState == null )
            changeFragment(new HomeFragment());

    }

    public void openMenu(View view){
        resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {

        }

        @Override
        public void closeMenu() {

        }


    };

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        if(v == itemHome){
            changeFragment(new HomeFragment());
        }
        else if(v == itemProfile){
            changeFragment(new ProfileFragment());
        }
        else if(v == logout){
            pref.edit().clear().apply();
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        resideMenu.closeMenu();
    }

    private void changeFragment(Fragment targetFragment){

        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }

//    @OnClick(R.id.btn_bottom_sheet)
//    public void showBottomSheetDialog() {
//        View view = getLayoutInflater().inflate(R.layout.bottom_sheet, null);
//
//        BottomSheetDialog dialog = new BottomSheetDialog(this);
//        dialog.setContentView(view);
//        dialog.show();
//    }

    public void showModal(View v){
//        changeFragment(new PostTweet());
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet, null);
        tweet = view.findViewById(R.id.tweetText);

        dialog.setContentView(view);
        dialog.show();
    }

    public void postTweet(View v){
        JSONObject body = new JSONObject();
        try {
            body.put("user_id", pref.getString("userId",""));
            Log.d("userIDnya", "onClick: "+pref.getString("userId",""));
            body.put("tweet", tweet.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String uri = "http://117.53.45.240/pnjtweet-backend/api/tweets";
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                uri,body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("", "onResponse: response = "+response.getString("tweet"));
                            dialog.dismiss();
                            changeFragment(new HomeFragment());
                            Toast.makeText(MainActivity.this,"Tweet berhasil di post",Toast.LENGTH_SHORT);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                            changeFragment(new HomeFragment());
                            Toast.makeText(MainActivity.this,"Tweet gagal di post",Toast.LENGTH_SHORT);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Failure Callback
                        Log.d("postTweet", "onErrorResponse: "+error);
                        dialog.dismiss();
                        changeFragment(new HomeFragment());
                        Toast.makeText(MainActivity.this,"Tweet gagal di post",Toast.LENGTH_SHORT);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);
                String token = pref.getString("token", "");
                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(req);
    }

    public ResideMenu getResideMenu(){
        return resideMenu;
    }
}
