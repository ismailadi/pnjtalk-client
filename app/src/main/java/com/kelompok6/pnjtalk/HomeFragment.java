package com.kelompok6.pnjtalk;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kelompok6.pnjtalk.Adapter.TweetAdapter;
import com.kelompok6.pnjtalk.Classes.Tweet;
import com.special.ResideMenu.ResideMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment {
    SharedPreferences pref;
    private View parentView;
    private ResideMenu resideMenu;
    private RecyclerView recyclerView;
    private TweetAdapter adapter;
    private ArrayList<Tweet> tweetArrayList;
    private SwipeRefreshLayout swipeContainer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_home, container, false);
        swipeContainer = parentView.findViewById(R.id.swipeContainer);
        pref = this.getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        setRefreshListener();
        setUpViews();
        setData();
        recyclerView = parentView.findViewById(R.id.recycler_view);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);

        adapter = new TweetAdapter(tweetArrayList);
        recyclerView.setAdapter(adapter);

        return parentView;
    }

    private void setRefreshListener(){
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                setData();
                adapter = new TweetAdapter(tweetArrayList);
                recyclerView.setAdapter(adapter);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setData(){
        tweetArrayList = new ArrayList<>();
        String uri = "http://117.53.45.240/pnjtweet-backend/api/tweets";
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,uri,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");

                    Log.d("Data", "setData: "+data.length());
                    for (int i = 0; i < data.length(); i++) {
                        try {
                            JSONObject jsonObject = data.getJSONObject(i);

                            Tweet tweet = new Tweet();
                            tweet.setName(jsonObject.getString("name"));
                            tweet.setUsername(jsonObject.getString("username"));
                            tweet.setCreatedAt(jsonObject.getString("created_at"));
                            tweet.setTweet(jsonObject.getString("tweet"));
                            tweet.setProfilePicture(jsonObject.getString("profile_picture"));
                            tweetArrayList.add(tweet);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);
                String token = pref.getString("token", "");
                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        swipeContainer.setRefreshing(false);
    }

    private void setUpViews() {
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }

}
