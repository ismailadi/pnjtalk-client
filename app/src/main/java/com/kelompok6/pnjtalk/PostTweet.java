package com.kelompok6.pnjtalk;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.special.ResideMenu.ResideMenu;

public class PostTweet extends Fragment {
    private View parentView;
    private ResideMenu resideMenu;
    RequestQueue requestQueue;
    SharedPreferences pref;
    EditText tweet;
    Button post;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_post_tweet, container, false);
        requestQueue = Volley.newRequestQueue(getActivity());
        tweet = (EditText) getActivity().findViewById(R.id.text_tweet);
        post = (Button) getActivity().findViewById(R.id.posttweet);
        pref = this.getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        setUpViews();
        return parentView;
    }

    private void setUpViews() {
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }

}
