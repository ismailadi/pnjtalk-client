package com.kelompok6.pnjtalk.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelompok6.pnjtalk.Classes.Tweet;
import com.kelompok6.pnjtalk.R;
import com.kelompok6.pnjtalk.Utils.DownloadImage;

import java.util.ArrayList;

public class TweetAdapter extends RecyclerView.Adapter<TweetAdapter.TweetViewHolder> {

    private ArrayList<Tweet> dataList;

    public TweetAdapter(ArrayList<Tweet> dataList) {
        this.dataList = dataList;
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.tweet_card, parent, false);
        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        holder.name.setText(dataList.get(position).getName());
        holder.username.setText("  @"+dataList.get(position).getUsername());
        holder.tweet.setText(dataList.get(position).getTweet());
        holder.created.setText(dataList.get(position).getCreatedAt());
        if(dataList.get(position).getProfilePicture() != "null"){
            new DownloadImage(holder.pp).execute(dataList.get(position).getProfilePicture());
        }
        else{
            holder.pp.setImageResource(R.drawable.ic_person_black_24dp);
//            new DownloadImage(holder.pp).execute("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973461_960_720.png");
        }
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder{
        private TextView name, username, tweet, created;
        private ImageView pp;
        public TweetViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            username = (TextView) itemView.findViewById(R.id.username);
            tweet = (TextView) itemView.findViewById(R.id.tweet);
            created = (TextView) itemView.findViewById(R.id.created);
            pp = itemView.findViewById(R.id.profilePhoto);
        }
    }

}
