package com.kelompok6.pnjtalk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kelompok6.pnjtalk.Utils.DownloadImage;
import com.special.ResideMenu.ResideMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileFragment extends Fragment {
    public static final String CACHED_IMG_KEY = "img_key";

    public static final int SECOND_PIC_REQ = 1313;
    public static final int GALLERY_ONLY_REQ = 1212;

    private View parentView;
    private ResideMenu resideMenu;
    SharedPreferences pref;

    ImageView bg,pp;
    TextView username;
    EditText tweet,name,password,identityId,bio;
    Button updateButton;
    RequestQueue requestQueue;
    String id,uri;
    Uri url;

    public ProfileFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_profile, container, false);
        pref = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        bg = (ImageView) parentView.findViewById(R.id.backgroundImage);
        pp = (ImageView) parentView.findViewById(R.id.profilePicture);
        username = parentView.findViewById(R.id.username);
        name = parentView.findViewById(R.id.name);
        password = parentView.findViewById(R.id.password);
        identityId = parentView.findViewById(R.id.id);
        bio = parentView.findViewById(R.id.bio);
        updateButton = parentView.findViewById(R.id.updateButton);
        requestQueue = Volley.newRequestQueue(getActivity());

        setUpViews();
        setData();
        requestAppPermissions();

        bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("image", "onClick: bg");
                Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
                intent.putExtra("param","coverpicture");
                startActivity(intent);
            }
        });

//        String path = pref.getString(CACHED_IMG_KEY, "");
//        File cached = new File(path);
//        if (cached.exists()) {
//            Picasso.get().load(cached).into(pp);
//        }

        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("image", "onClick: pp");
                Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
                intent.putExtra("param","profilepicture");
                startActivity(intent);
            }
        });

        setListener();

        return parentView;
    }

    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(MainActivity.mainView, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void setListener(){
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });
    }

    private void setUpViews() {
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }

    private void setData(){
        Log.d("get", "setData: url"+pref.getString("userId",""));
        String uri = "http://117.53.45.240/pnjtweet-backend/api/users?id="+pref.getString("userId","");
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,uri,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject data = response.getJSONArray("data").getJSONObject(0);
                    username.setText("@"+data.getString("username"));
                    if(data.getString("profile_picture") != "null" && data.getString("cover_picture") != null){
                        new DownloadImage(pp).execute(data.getString("profile_picture"));
                        new DownloadImage(bg).execute(data.getString("cover_picture"));
                    }
                    else if(data.getString("profile_picture") != "null" && data.getString("cover_picture") == null ){
                        new DownloadImage(pp).execute(data.getString("profile_picture"));
                    }
                    else if(data.getString("profile_picture") == "null" && data.getString("cover_picture") != null ){
                        new DownloadImage(bg).execute(data.getString("cover_picture"));
                    }
                    name.setText(data.getString("name"));
                    password.setText(data.getString("password"));
                    identityId.setText(data.getString("identity_id"));
                    bio.setText(data.getString("bio"));
                    Log.d("Data", "setData: "+data.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);
                String token = pref.getString("token", "");
                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
    }

    public void updateProfile(){
        JSONObject body = new JSONObject();
        try {
            body.put("id", pref.getString("userId",""));
            body.put("name", name.getText());
            body.put("password", password.getText());
            body.put("identity_id", identityId.getText());
            body.put("bio", bio.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String uri = "http://117.53.45.240/pnjtweet-backend/api/users";
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT,
                uri,body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("updateProfile", "onResponse: response = "+response.getBoolean("status"));
                            Log.d("updateProfile", "onResponse: id = "+response.getJSONObject("data").getString("id"));
                            Log.d("updateProfile", "onResponse: name = "+response.getJSONObject("data").getString("name"));
                            Log.d("updateProfile", "onResponse: password = "+response.getJSONObject("data").getString("password"));
                            Log.d("updateProfile", "onResponse: identity = "+response.getJSONObject("data").getString("identity_id"));
                            Log.d("updateProfile", "onResponse: bio = "+response.getJSONObject("data").getString("bio"));
                            Toast.makeText(getActivity(),"Profile berhasil diubah",Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
//                            dialog.dismiss();
//                            changeFragment(new HomeFragment());
                            Toast.makeText(getActivity(),"Profile gagal diubah",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Failure Callback
                        Log.d("updateProfile", "onErrorResponse: "+error);
//                        dialog.dismiss();
//                        changeFragment(new HomeFragment());
                        Toast.makeText(getActivity(),"Profile gagal diubah",Toast.LENGTH_SHORT);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);
                String token = pref.getString("token", "");
                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(req);
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(getActivity(),
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }
}
