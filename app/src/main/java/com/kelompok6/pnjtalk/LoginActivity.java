package com.kelompok6.pnjtalk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "MyPref";
    SharedPreferences.Editor editor;
    RequestQueue requestQueue;

    // UI references.
    private EditText email;
    private EditText password;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        email = (EditText) findViewById(R.id.textEmail);
        password = (EditText) findViewById(R.id.textPassword);

        requestQueue = Volley.newRequestQueue(LoginActivity.this);
//        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
////                    attemptLogin();
//                    return true;
//                }
//                return false;
//            }
//        });

        Button mEmailSignInButton = (Button) findViewById(R.id.buttonLogin);

    }

    public void login(View view) {
        JSONObject body = new JSONObject();
        try {
            body.put("username", email.getText());
            body.put("password", password.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String uri = "http://117.53.45.240/pnjtweet-backend/api/auth/login";
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                uri,body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");

                            Log.d("login", "onResponse: "+status);
                            if(status){
                                String token = response.getString("token");
                                editor.putString("username", String.valueOf(email.getText()));
                                editor.putString("token", token);
                                getUser(String.valueOf(email.getText()),token);
//                                Thread.sleep(1000);
                                Log.d("login", "onResponse: token = "+token);
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Username or password wrong", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(LoginActivity.this, "Ups sorry.. Something happened.. Failed to login", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
//                        catch (InterruptedException e) {
//                            Toast.makeText(LoginActivity.this, "Ups sorry.. Something happened.. Failed to login", Toast.LENGTH_SHORT).show();
//                            e.printStackTrace();
//                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Failure Callback
                        Log.d("login", "onErrorResponse: "+error);
                        Toast.makeText(LoginActivity.this, "Ups sorry.. Something happened.. Failed to login", Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(req);

    }

    private void getUser(String username, final String token){
        String uri = "http://117.53.45.240/pnjtweet-backend/api/users?username="+username;
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET,uri,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    editor.putString("userId", response.getJSONArray("data").getJSONObject(0).getString("id"));
                    editor.apply();

                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Thread.sleep(1000);
                    startActivity(intent);
                    finish();
                    Log.d("userid", "onResponse: UserID "+response.getJSONArray("data").getJSONObject(0).getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // Basic Authentication
                //String auth = "Basic " + Base64.encodeToString(CONSUMER_KEY_AND_SECRET.getBytes(), Base64.NO_WRAP);

                Log.d("token", "getHeaders: "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };;
        requestQueue.add(jsonArrayRequest);
    }
}

